import yaml
import uuid
import json
import requests
import urllib.parse
from flask_oidc import OpenIDConnect
import os
import logging
from flask import Flask, render_template, redirect, request
from requests.sessions import session
from flask_restful import Api
import yaml
import sys

app = Flask(__name__)

app.config.update({
    'SECRET_KEY': os.environ.get('OIDC_SECRET'),
    'TESTING': True,
    'DEBUG': True,
    'OIDC_CLIENT_SECRETS': os.environ.get('IODC_CLIENT_SECRETS_PATH'),
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_OPENID_REALM': os.environ.get('KEYCLOAK_REALM'),
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token'
})
oidc = OpenIDConnect(app)

api = Api(app)

if os.environ.get('VAST_URL') is not None:
    vast_url = "https://"+os.environ.get("VAST_URL")
else:
    vast_url = None


def get_parameters(yaml_node, parameters, path):
    if type(yaml_node) == list:
        i = 0
        for item in yaml_node:
            get_parameters(
                item, parameters, path+"["+str(i)+"]")
            i = i+1
    else:
        for key, value in yaml_node.items():
            if type(value) == dict or type(value) == list:
                if path == "":
                    new_path = key
                else:
                    new_path = path+"["+key+"]"
                get_parameters(
                    value, parameters, new_path)
            elif key == "path":
                parameters[path] = "file"


@app.route('/')
def index():
    return render_template('home.html')


@app.route('/launch_predefined', methods=['GET'])
@oidc.require_login
def launch_predefined_get():
    with open("./config.json", "r") as fp:
        config = json.load(fp=fp)
        logging.error(config['workflows'])
    return render_template('launch_predefined.html', workflows=config['workflows'])


@app.route('/launch', methods=['GET'])
@oidc.require_login
def launch_get():
    return render_template('launch.html')


@app.route('/launch', methods=['POST'])
@oidc.require_login
def launch_post():
    token = oidc.get_access_token()
    if not token:
        oidc.logout()
        return render_template('home.html')
    h = dict(request.headers)
    h_new = {}
    h_new['Authorization'] = f"Bearer {token}"
    h_new['Content-Type'] = h['Content-Type']
    r = requests.post("https://rest.service-test.oncopmnet.gr/launch",
                      data=request.stream, headers=h_new)
    return r.content.decode('UTF-8')


@app.route('/extract_parameters', methods=['POST'])
@oidc.require_login
def extract_parameters_post():
    data = {}
    if 'inputs_url' in request.form and request.form['inputs_url'] != "":
        r = requests.get(request.form['inputs_url'])
        if not r.ok:
            err = "Trying to reach inputs url: " + \
                request.form['inputs_url'] + " returned: " + str(r.status_code)
            logging.error(err)
            return "{'error': 'Cannot download yaml from url'}"
        read = r.content
        data = yaml.load(read, Loader=yaml.SafeLoader)
        logging.error(data)
    else:
        dir_path = "./storage/"
        this_uuid = str(uuid.uuid4())
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        inputs = dict((k, v) for k, v in request.files.items() if v)
        yaml_path = dir_path + this_uuid + \
            "_" + inputs['inputs_local'].filename
        try:
            inputs['inputs_local'].save(yaml_path)
        except Exception as err:
            logging.error(err)
            return "{'error': 'Cannot load file'}"
        with open(yaml_path, 'rb') as file:
            data = yaml.load(file, Loader=yaml.SafeLoader)
        try:
            os.remove(yaml_path)
        except Exception as err:
            logging.error(err)
    parameters = {}
    get_parameters(data, parameters, "")
    return parameters


@ app.route('/status', methods=['GET'])
@ oidc.require_login
def status_get():
    return render_template('status.html')


@ app.route('/status', methods=['POST'])
@ oidc.require_login
def status_post():
    token = oidc.get_access_token()
    if not token:
        oidc.logout()
        return render_template('home.html')
    h = dict(request.headers)
    h_new = {}
    h_new['Authorization'] = f"Bearer {token}"
    h_new['Content-Type'] = h['Content-Type']
    r = requests.post("https://rest.service-test.oncopmnet.gr/status",
                      data=request.stream, headers=h_new)
    return render_template('status.html', status=r.content.decode('UTF-8'))


@ app.route('/metadata', methods=['GET'])
@ oidc.require_login
def metadata_get():
    return render_template('metadata.html')


@ app.route('/metadata', methods=['POST'])
@ oidc.require_login
def metadata_post():
    token = oidc.get_access_token()
    if not token:
        oidc.logout()
        return render_template('home.html')
    h = dict(request.headers)
    h_new = {}
    h_new['Authorization'] = f"Bearer {token}"
    h_new['Content-Type'] = h['Content-Type']
    r = requests.post("https://rest.service-test.oncopmnet.gr/metadata",
                      data=request.stream, headers=h_new)
    metadata = json.loads(r.content.decode('UTF-8'))
    return render_template('metadata.html', metadata=json.dumps(metadata, indent=4, sort_keys=True))


@ app.route('/runs', methods=['GET'])
@ oidc.require_login
def runs_get():
    token = oidc.get_access_token()
    if not token:
        oidc.logout()
        return render_template('home.html')
    h_new = {}
    h_new['Authorization'] = f"Bearer {token}"
    r = requests.get(
        "https://rest.service-test.oncopmnet.gr/runs", headers=h_new)
    runs = json.loads(r.content.decode('UTF-8'))
    return render_template('runs.html', runs=json.dumps(runs, indent=4, sort_keys=True))


@ app.route('/outputs', methods=['GET'])
@ oidc.require_login
def outputs_get():
    return render_template('outputs.html')


@ app.route('/outputs', methods=['POST'])
@ oidc.require_login
def outputs_post():
    token = oidc.get_access_token()
    if not token:
        oidc.logout()
        return render_template('home.html')
    h = dict(request.headers)
    h_new = {}
    h_new['Authorization'] = f"Bearer {token}"
    h_new['Content-Type'] = h['Content-Type']
    r = requests.post("https://rest.service-test.oncopmnet.gr/outputs",
                      data=request.stream, headers=h_new, stream=True)
    if 'Content-Disposition' in r.headers:
        r = app.response_class(r.iter_content(
            chunk_size=8192), mimetype='application/zip')
        r.headers.set('Content-Disposition', 'attachment',
                      filename='outputs.zip')
        return r
    else:
        return render_template('outputs.html', status=r.content.decode('UTF-8'))


@ app.route('/cancel', methods=['GET'])
@ oidc.require_login
def cancel_get():
    return render_template('cancel.html')


@ app.route('/cancel', methods=['POST'])
@ oidc.require_login
def cancel_post():
    token = oidc.get_access_token()
    if not token:
        oidc.logout()
        return render_template('home.html')
    h = dict(request.headers)
    h_new = {}
    h_new['Authorization'] = f"Bearer {token}"
    h_new['Content-Type'] = h['Content-Type']
    r = requests.post("https://rest.service-test.oncopmnet.gr/cancel",
                      data=request.stream, headers=h_new)
    return render_template('cancel.html', status=r.content.decode('UTF-8'))


@ app.route('/get_token', methods=['GET'])
@ oidc.require_login
def get_token():
    token = oidc.get_access_token()
    if not token:
        oidc.logout()
        return render_template('home.html')
    return render_template('authenticate.html', token=token)


@app.route('/checkorderid', methods=['POST'])
@oidc.require_login
def check_orderid_post():
    token = oidc.get_access_token()
    if not token:
        oidc.logout()
        return render_template('home.html')

    # check if order id is valid
    import datetime
    import re

    orderId = request.form['orderId']
    pattern1 = "^[0-9]{4}-[0-9]{5}-[0-9]{4}$"
    pattern2 = "^[0-9]{13}$"
    if not bool(re.match(pattern1, orderId)):
        if not bool(re.match(pattern2, orderId)):
            return "not_valid"
        try:
            datetime.datetime.strptime(
                "20" + orderId[0:2] + "-" + orderId[2:4] + "-" + orderId[4:6], '%Y-%m-%d')
        except ValueError:
            return "not_valid"

    h = dict(request.headers)
    h_new = {}
    h_new['Authorization'] = f"Bearer {token}"
    payload = {'orderId': request.form['orderId']}
    r = requests.post("https://rest.service-test.oncopmnet.gr/metadata",
                      data=payload, headers=h_new)
    data = r.json()
    if "message" in data:
        return "not_taken"
    else:
        runs = data['results']
        for run in runs:
            status = run[list(run.keys())[0]]['status']
            if status == "Running" or status == "Submitted" or status == "Succeeded":
                return "taken"

    return "not_taken"


@ app.route('/logout', methods=['GET'])
def logout():
    oidc.logout()
    return redirect(oidc.client_secrets['logout_uri']+'?' +
                    urllib.parse.urlencode({'redirect_uri': request.host_url}))


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


if __name__ == '__main__':
    app.run(debug=True)
