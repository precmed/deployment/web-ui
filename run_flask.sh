#!/bin/sh

export UI_WORKERS="${UI_WORKERS:-1}"
export UI_THREADS="${UI_THREADS:-50}"

gunicorn -b 0.0.0.0:8000 app:app --workers=$UI_WORKERS --threads=$UI_THREADS --timeout 8000 --log-level debug 