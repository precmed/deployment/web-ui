FROM python:3.6-alpine3.14

COPY requirements.txt /app/

WORKDIR /app

RUN pip install -r requirements.txt

RUN apk add --no-cache \
    yaml-dev \
    zip

COPY templates /app/templates
COPY app.py run_flask.sh config.json /app/ 

RUN chmod 755 /app/run_flask.sh

EXPOSE 8000

CMD /app/run_flask.sh
